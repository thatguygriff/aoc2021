package thirteen

import (
	"fmt"
	"testing"
)

func Test_read(t *testing.T) {
	m := manual{}
	if err := m.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	dots := countDots(m.dots)
	folds := len(m.folds)

	if dots != 18 {
		t.Logf("Expected 18 dots, found %d", dots)
		t.Fail()
	}

	if folds != 2 {
		t.Logf("Expected 2 folds, found %d", folds)
		t.Fail()
	}

	x, y := gridSize(m.dots)

	if x != 11 || y != 15 {
		t.Logf("Expected 11x15, found %dx%d", x, y)
		t.Fail()
	}
}

func Test_fold(t *testing.T) {
	m := manual{}
	m.load("test_input.txt")

	printGrid(m.dots)
	fmt.Println("")
	grid, dots := foldGrid(m.folds[0], m.dots)

	printGrid(grid)
	if dots != 17 {
		t.Logf("Expected 17 dots after fold, found %d", dots)
		t.Fail()
	}

	grid, dots = foldGrid(m.folds[1], grid)

	fmt.Println("")
	printGrid(grid)
	if dots != 16 {
		t.Logf("Expected 16 dots after fold, found %d", dots)
		t.Fail()
	}
}
