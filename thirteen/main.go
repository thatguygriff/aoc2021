package thirteen

import "fmt"

type Thirteen struct {
	manual manual
}

func Init(filepath string) *Thirteen {
	thirteen := &Thirteen{
		manual: manual{},
	}

	thirteen.manual.load(filepath)
	return thirteen
}

func (d *Thirteen) Answer() string {
	_, dots := foldGrid(d.manual.folds[0], d.manual.dots)
	return fmt.Sprintf("There are %d dots after 1 fold", dots)
}

func (d *Thirteen) FollowUp() string {
	d.manual.fold()
	return "This is the activation code"
}
