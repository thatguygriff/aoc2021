package nineteen

type Nineteen struct {
	mapping mapping
}

func Init(filepath string) *Nineteen {
	nineteen := &Nineteen{
		mapping: mapping{},
	}

	nineteen.mapping.load(filepath)
	return nineteen
}

func (d *Nineteen) Answer() string {
	return "nineteen part 1 answer for mapping"
}

func (d *Nineteen) FollowUp() string {
	return "nineteen part 2 answer for mapping"
}

