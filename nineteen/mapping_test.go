package nineteen

import "testing"

func Test_read(t *testing.T) {
	m := mapping{}
	if err := m.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(m.scanners) != 5 {
		t.Logf("Expected 5 scanners, found %d", len(m.scanners))
		t.Fail()
	}

	if len(m.scanners[0].beacons) != 25 {
		t.Logf("Expected 25 beacons on scanner %d, found %d", m.scanners[0].id, len(m.scanners[0].beacons))
		t.Fail()
	}

	if len(m.scanners[1].beacons) != 25 {
		t.Logf("Expected 25 beacons on scanner %d, found %d", m.scanners[1].id, len(m.scanners[1].beacons))
		t.Fail()
	}

	if len(m.scanners[2].beacons) != 26 {
		t.Logf("Expected 26 beacons on scanner %d, found %d", m.scanners[2].id, len(m.scanners[2].beacons))
		t.Fail()
	}

	if len(m.scanners[3].beacons) != 25 {
		t.Logf("Expected 25 beacons on scanner %d, found %d", m.scanners[3].id, len(m.scanners[3].beacons))
		t.Fail()
	}

	if len(m.scanners[4].beacons) != 26 {
		t.Logf("Expected 26 beacons on scanner %d, found %d", m.scanners[4].id, len(m.scanners[4].beacons))
		t.Fail()
	}
}

func Test_initialize(t *testing.T) {
	m := mapping{}
	m.load("test_input.txt")

	m.initialize()

	beaconCount := len(m.beacons)

	if beaconCount != 79 {
		t.Logf("Expected 79 beacons, found %d", beaconCount)
		// t.Fail()
	}
}
