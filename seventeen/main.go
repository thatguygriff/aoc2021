package seventeen

import "fmt"

type Seventeen struct {
	probe probe
}

func Init(filepath string) *Seventeen {
	seventeen := &Seventeen{
		probe: probe{},
	}

	seventeen.probe.load(filepath)
	return seventeen
}

func (d *Seventeen) Answer() string {
	ivs := d.probe.intialVelocities()

	return fmt.Sprintf("The max height is %d", maxHeight(d.probe, ivs))
}

func (d *Seventeen) FollowUp() string {
	ivs := d.probe.intialVelocities()

	return fmt.Sprintf("There are %d initial velocities that will hit the target", len(ivs))
}
