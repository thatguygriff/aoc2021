package seventeen

import (
	"testing"
)

func Test_read(t *testing.T) {
	p := probe{}
	if err := p.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if p.target.xMin != 20 {
		t.Logf("Expected 20, found %d", p.target.xMin)
		t.Fail()
	}

	if p.target.xMax != 30 {
		t.Logf("Expected 30, found %d", p.target.xMax)
		t.Fail()
	}

	if p.target.yMax != -5 {
		t.Logf("Expected 20, found %d", p.target.xMin)
		t.Fail()
	}

	if p.target.yMin != -10 {
		t.Logf("Expected 30, found %d", p.target.xMax)
		t.Fail()
	}
}

func Test_initialVelocities(t *testing.T) {
	p := probe{}
	p.load("test_input.txt")

	ivs := p.intialVelocities()

	if len(ivs) != 112 {
		t.Logf("Expected 112 initial velocities to target, found %d", len(ivs))
		t.Fail()
	}
}

func Test_maxHeight(t *testing.T) {
	p := probe{}
	p.load("test_input.txt")

	ivs := p.intialVelocities()
	max := maxHeight(p, ivs)

	if max != 45 {
		t.Logf("Expected max height of 45, found %d", max)
		t.Fail()
	}
}
