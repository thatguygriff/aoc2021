package seven

import "testing"

func Test_read(t *testing.T) {
	c := crabs{}
	if err := c.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if c.count != 10 {
		t.Logf("Expected 10 crabs, found %d", c.count)
		t.Fail()
	}

}

func Test_align(t *testing.T) {
	c := crabs{}
	c.load("test_input.txt")

	fuelUsed := c.align(false)
	if fuelUsed != 37 {
		t.Logf("Expected 37 fuel used to align, found %d", fuelUsed)
		t.Fail()
	}
}

func Test_alignNonConstant(t *testing.T) {
	c := crabs{}
	c.load("test_input.txt")

	fuelUsed := c.align(true)
	if fuelUsed != 168 {
		t.Logf("Expected 168 fuel used to align, found %d", fuelUsed)
		t.Fail()
	}
}