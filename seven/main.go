package seven

import "fmt"

type Seven struct {
	crabs crabs
}

func Init(filepath string) *Seven {
	seven := &Seven{
		crabs: crabs{},
	}

	seven.crabs.load(filepath)
	return seven
}

func (d *Seven) Answer() string {
	fuelUsed := d.crabs.align(false)
	return fmt.Sprintf("Used %d fuel to align", fuelUsed)
}

func (d *Seven) FollowUp() string {
	fuelUsed := d.crabs.align(true)
	return fmt.Sprintf("Used %d fuel to align", fuelUsed)
}
