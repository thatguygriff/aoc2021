package six

import "fmt"

type Six struct {
	lanternfish lanternfish
}

func Init(filepath string) *Six {
	six := &Six{
		lanternfish: lanternfish{},
	}

	six.lanternfish.load(filepath)
	return six
}

func (d *Six) Answer() string {
	d.lanternfish.simulate(80)
	return fmt.Sprintf("After 80 days there are %d fish", d.lanternfish.population)
}

func (d *Six) FollowUp() string {
	d.lanternfish.simulate(176)
	return fmt.Sprintf("After 256 days there are %d fish", d.lanternfish.population)
}
