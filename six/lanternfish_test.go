package six

import "testing"

func Test_read(t *testing.T) {
	l := lanternfish{}
	if err := l.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if l.population != 5 {
		t.Logf("Expected 5 fish, found %d", l.population)
		t.Fail()
	}
}

func Test_simulate(t *testing.T) {
	l := lanternfish{}
	l.load("test_input.txt")

	l.simulate(18)
	if l.population != 26 {
		t.Logf("Expected 26 fish, found %d", l.population)
		t.Fail()
	}

	l.simulate(62)
	if l.population != 5934 {
		t.Logf("Expected 5934 fish, found %d", l.population)
		t.Fail()
	}
}

func Test_simulateForever(t *testing.T) {
	l := lanternfish{}
	l.load("test_input.txt")

	l.simulate(256)
	if l.population != 26984457539 {
		t.Logf("Expected 26984457539 fish, found %d", l.population)
		t.Fail()
	}
}
