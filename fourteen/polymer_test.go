package fourteen

import "testing"

func Test_read(t *testing.T) {
	p := polymer{}
	if err := p.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if p.template != "NNCB" {
		t.Logf("Expected template of NNCB, found %s", p.template)
		t.Fail()
	}

	if len(p.rules) != 16 {
		t.Logf("Expected 16 rules, found %d", len(p.rules))
		t.Fail()
	}
}

func Test_step(t *testing.T) {
	p := polymer{}
	p.load("test_input.txt")

	if result := p.step(1); result != "NCNBCHB" {
		t.Logf("Expected NCNBCHB, found %s", result)
		t.Fail()
	}

	if result := p.step(2); result != "NBCCNBBBCBHCB" {
		t.Logf("Expected NBCCNBBBCBHCB, found %s", result)
		t.Fail()
	}

	if result := p.step(3); result != "NBBBCNCCNBBNBNBBCHBHHBCHB" {
		t.Logf("Expected NBBBCNCCNBBNBNBBCHBHHBCHB, found %s", result)
		t.Fail()
	}

	if result := p.step(4); result != "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB" {
		t.Logf("Expected NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB, found %s", result)
		t.Fail()
	}

}

func Test_elemCount(t *testing.T) {
	p := polymer{}
	p.load("test_input.txt")

	if result := p.elementCount(10); result != 1588 {
		t.Logf("Expected a result of 1588, got %d", result)
		t.Fail()
	}
}

func Test_elemMapCount(t *testing.T) {
	p := polymer{}
	p.load("test_input.txt")

	if result := p.elementCountMap(10); result != 1588 {
		t.Logf("Expected a result of 1588, got %d", result)
		t.Fail()
	}

	if result := p.elementCountMap(40); result != 2188189693529 {
		t.Logf("Expected a result of 2188189693529, got %d", result)
		t.Fail()
	}
}
