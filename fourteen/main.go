package fourteen

import "fmt"

type Fourteen struct {
	polymer polymer
}

func Init(filepath string) *Fourteen {
	fourteen := &Fourteen{
		polymer: polymer{},
	}

	fourteen.polymer.load(filepath)
	return fourteen
}

func (d *Fourteen) Answer() string {
	return fmt.Sprintf("The result of subtracting the least from most common element quantities is %d", d.polymer.elementCount(10))
}

func (d *Fourteen) FollowUp() string {
	return fmt.Sprintf("The result of subtracting the least from most common element quantities is %d", d.polymer.elementCountMap(40))
}
