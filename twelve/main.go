package twelve

import "fmt"

type Twelve struct {
	navigation navigation
}

func Init(filepath string) *Twelve {
	twelve := &Twelve{
		navigation: navigation{},
	}

	twelve.navigation.load(filepath)
	return twelve
}

func (d *Twelve) Answer() string {
	return fmt.Sprintf("Found %d paths through the cave system", len(d.navigation.findPaths()))
}

func (d *Twelve) FollowUp() string {
	return fmt.Sprintf("Found %d paths through the cave system", len(d.navigation.findPaths2()))
}
