package twelve

import (
	"testing"
)

func Test_read(t *testing.T) {
	n := navigation{}
	if err := n.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(n.input) != 7 {
		t.Logf("Expected 7 inputs, found %d", len(n.input))
		t.Fail()
	}

	if len(n.caves) != 6 {
		t.Logf("Expected 6 caves, found %d", len(n.caves))
		t.Fail()
	}
}

func Test_findPathsInput1(t *testing.T) {
	n := navigation{}
	n.load("test_input.txt")

	paths := n.findPaths()

	if len(paths) != 10 {
		t.Logf("Expected 10 paths, found %d", len(paths))
		t.Fail()
	}

	paths = n.findPaths2()

	if len(paths) != 36 {
		t.Logf("Expected 36 paths, found %d", len(paths))
		t.Fail()
	}
}

func Test_findPathsInput2(t *testing.T) {
	n := navigation{}
	n.load("test_input2.txt")

	paths := n.findPaths()

	if len(paths) != 19 {
		t.Logf("Expected 19 paths, found %d", len(paths))
		t.Fail()
	}

	paths = n.findPaths2()

	if len(paths) != 103 {
		t.Logf("Expected 103 paths, found %d", len(paths))
		t.Fail()
	}
}

func Test_findPathsInput3(t *testing.T) {
	n := navigation{}
	n.load("test_input3.txt")

	paths := n.findPaths()

	if len(paths) != 226 {
		t.Logf("Expected 226 paths, found %d", len(paths))
		t.Fail()
	}

	paths = n.findPaths2()

	if len(paths) != 3509 {
		t.Logf("Expected 3509 paths, found %d", len(paths))
		t.Fail()
	}
}
