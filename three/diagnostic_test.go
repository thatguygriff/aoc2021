package three

import "testing"

var test_input = []string{
	"00100",
	"11110",
	"10110",
	"10111",
	"10101",
	"01111",
	"00111",
	"11100",
	"10000",
	"11001",
	"00010",
	"01010",
}

func Test_read(t *testing.T) {
	r := report{}
	if err := r.read("input.txt"); err != nil {
		t.FailNow()
	}

	if len(r.values) != 1000 {
		t.Logf("Expected 1000 values, found %d", len(r.values))
		t.Fail()
	}
}

func Test_computeCO2Rating(t *testing.T) {
	r := report{
		values: test_input,
	}

	r.computeCO2Rating()

	if r.co2Rating != 10 {
		t.Logf("Expected a CO2 rating of 10, got %d", r.co2Rating)
		t.Fail()
	}
}