package three

import "fmt"

type Three struct {
	report report
}

func Init(filepath string) *Three {
	three := &Three{
		report: report{},
	}

	three.report.read(filepath)

	return three
}

func (d *Three) Answer() string {
	d.report.computePowerConsumption()
	return fmt.Sprintf("The power rates are gamma: %d, epsilon: %d, for %d total", d.report.gammaRate, d.report.epsilonRate, d.report.gammaRate*d.report.epsilonRate)
}

func (d *Three) FollowUp() string {
	d.report.computeOxygenRating()
	d.report.computeCO2Rating()

	return fmt.Sprintf(
		"The oxygen rating is %d and CO2 rating is %d, for a life support rating of %d",
		d.report.oxygenRating,
		d.report.co2Rating,
		d.report.oxygenRating * d.report.co2Rating,
	)
}
