package three

import (
	"bufio"
	"os"
)

type report struct {
	gammaRate    int
	epsilonRate  int
	oxygenRating int
	co2Rating    int
	values       []string
}

func (r *report) read(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		r.values = append(r.values, scanner.Text())
	}

	return nil
}

func (r *report) computePowerConsumption() {
	if len(r.values) == 0 {
		return
	}

	inputLength := len(r.values[0])

	// Loop over every bit of the input
	for i := 0; i < inputLength; i++ {
		ones := 0
		zeros := 0

		// Loop over every value to determine the most and least common bit values
		for _, value := range r.values {
			if value[i] == '1' {
				ones++
			} else {
				zeros++
			}
		}

		if ones > zeros {
			r.gammaRate = r.gammaRate<<1 + 1
			r.epsilonRate = r.epsilonRate<<1 + 0
		} else {
			r.gammaRate = r.gammaRate<<1 + 0
			r.epsilonRate = r.epsilonRate<<1 + 1
		}
	}
}

func (r *report) computeOxygenRating() {
	if len(r.values) == 0 {
		return
	}

	inputLength := len(r.values[0])

	filtered := r.values
	// Loop over every bit of the input
	for i := 0; i < inputLength; i++ {
		if len(filtered) == 1 {
			break
		}

		ones := []string{}
		zeros := []string{}

		// Loop over every value to determine the most and least common bit values
		for _, value := range filtered {
			if value[i] == '1' {
				ones = append(ones, value)
			} else {
				zeros = append(zeros, value)
			}
		}

		if len(ones) >= len(zeros) {
			filtered = ones
		} else {
			filtered = zeros
		}
	}

	for _, b := range filtered[0] {
		if b == '1' {
			r.oxygenRating = r.oxygenRating<<1 + 1
		} else {
			r.oxygenRating = r.oxygenRating<<1 + 0
		}
	}
}

func (r *report) computeCO2Rating() {
	if len(r.values) == 0 {
		return
	}

	inputLength := len(r.values[0])

	filtered := r.values
	// Loop over every bit of the input
	for i := 0; i < inputLength; i++ {
		if len(filtered) == 1 {
			break
		}

		ones := []string{}
		zeros := []string{}

		// Loop over every value to determine the most and least common bit values
		for _, value := range filtered {
			if value[i] == '1' {
				ones = append(ones, value)
			} else {
				zeros = append(zeros, value)
			}
		}

		if len(ones) < len(zeros) {
			filtered = ones
		} else {
			filtered = zeros
		}
	}

	for _, b := range filtered[0] {
		if b == '1' {
			r.co2Rating = r.co2Rating<<1 + 1
		} else {
			r.co2Rating = r.co2Rating<<1 + 0
		}
	}
}
