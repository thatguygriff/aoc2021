# Advent of Code 2021

The 2021 iteration of Advent of Code. This year I am going to try and accomplish it all on my iPad using GitPod

## Adding a new day

When starting to solve for a new day run the following command in the root of the repo where `<day>` is the go lang compatible package name (i.e. `four` or `five`) and `<noun>` represents the item the day's problem revolves around (i.e. `radar` or `bingo`).

```sh
./setup.sh <day> <noun>
```

After generating a new package update [`main.go`](main.go) to include a `<day>` flag in the switch statement on line 28

```go
  case "<day>":
		day = <day>.Init("<day>/input.txt")
```

## My Solutions

These solutions are based on my input values provided to me. The inputs I was provided are also included in this repo.

### Day 1

```sh
$ ./aoc2021 --one
The solution for "one" is:
There were 1715 increases in depth
There were 1739 increases in depth when sampling a window
```

### Day 2

```sh
$ ./aoc2021 --two
The solution for "two" is:
Horizontal is 1832, depth is 1172, answer is 2147104
Horizontal is 1832, depth is 1116059, answer is 2044620088
```

### Day 3

```sh
$ ./aoc2021 --three
The solution for "three" is:
The power rates are gamma: 1143, epsilon: 2952, for 3374136 total
The oxygen rating is 1909 and CO2 rating is 2322, for a life support rating of 4432698
```

### Day 4

```sh
$ ./aoc2021 --four
The solution for "four" is:
The winning score is 668 on call 66. Final score 44088
The last winning score is 263 on call 90. Final score 23670
```

### Day five

```sh
$ ./aoc2021 --five
The solution for "five" is:
There are 5585 overlapping vents

There are 17193 overlapping vents
```

### Day six

```sh
$ ./aoc2021 --six
The solution for "six" is:
After 80 days there are 377263 fish
After 256 days there are 1695929023803 fish
```

### Day seven

```sh
$ ./aoc2021 --seven
The solution for "seven" is:
Used 357353 fuel to align
Used 104822130 fuel to align
```

### Day eight

```sh
$ ./aoc2021 --eight
The solution for "eight" is:
There were 476 instances of digits 1, 4, 7, or 8
The sum of the decoded output values was 1011823
```

### Day nine

```sh
$ ./aoc2021 --nine
The solution for "nine" is:
The sum of the risk level of low points is 439
The product of the three largest basins is 900900
```

### Day ten

```sh
$ ./aoc2021 --ten
The solution for "ten" is:
The syntax error score is 323613
The autoComplete score is 3103006161
```

### Day eleven

```sh
$ ./aoc2021 --eleven
The solution for "eleven" is:
There were 1669 flashes after 100 steps
They all flash on step 351
```

### Day twelve

```sh
$ ./aoc2021 --twelve
The solution for "twelve" is:
Found 5157 paths through the cave system
Found 144309 paths through the cave system
```

### Day thirteen

```sh
$ ./aoc2021 --thirteen
The solution for "thirteen" is:
There are 837 dots after 1 fold
#### ###  ####  ##  #  #  ##  #  # #  #
#    #  #    # #  # # #  #  # #  # #  #
###  #  #   #  #    ##   #    #### #  #
#    ###   #   # ## # #  #    #  # #  #
#    #    #    #  # # #  #  # #  # #  #
#### #    ####  ### #  #  ##  #  #  ##
This is the activation code
```

### Day fourteen

```sh
$ ./aoc2021 --fourteen
The solution for "fourteen" is:
The result of subtracting the least from most common element quantities is 2988
The result of subtracting the least from most common element quantities is 3572761917024
```

### Day fifteen

Today's solution is SLLLLLLOOOOOOOWWW (~75 minutes) but correct!

```sh
$ ./aoc2021 --fifteen
The solution for "fifteen" is:
The total risk of the least risky path is 415
The total risk of the least risky path is 2864
```

### Day sixteen

```sh
$ ./aoc2021 --sixteen
The solution for "sixteen" is:
The sum of the version numbers is 1012
The output of the encoded input is 2223947372407
```

### Day seventeen

```sh
$ ./aoc2021 --seventeen
The solution for "seventeen" is:
The max height is 35511
There are 3282 initial velocities that will hit the target
```

### Day eighteen

```sh
$ ./aoc2021 --eighteen
The solution for "eighteen" is:
The magnitude of the final sum is 4202
The largest magnitude of any two snail numbers is 4779
```

### Day nineteen

```sh
$ ./aoc2021 --nineteen
The solution for "nineteen" is:
nineteen part 1 answer for map
nineteen part 2 answer for map
```


### Day nineteen

```sh
$ ./aoc2021 --nineteen
The solution for "nineteen" is:
nineteen part 1 answer for mapping
nineteen part 2 answer for mapping
```

