#!/bin/bash

mkdir $1

name=$(echo $1 | awk '{print toupper(substr($0,1,1)) tolower(substr($0,2)) }')
abbr=$(echo $2 | awk '{print substr($0,1,1) }')

# Create day implementation
echo "package $1

type $name struct {
	$2 $2
}

func Init(filepath string) *$name {
	$1 := &$name{
		$2: $2{},
	}

	$1.$2.load(filepath)
	return $1
}

func (d *$name) Answer() string {
	return \"$1 part 1 answer for $2\"
}

func (d *$name) FollowUp() string {
	return \"$1 part 2 answer for $2\"
}
" >> $1/main.go

# Create file for the day
echo "package $1
import (
	\"bufio\"
	\"os\"
)

type $2 struct {
  input []string
}

func ($abbr *$2) load(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
    $abbr.input = append($abbr.input, scanner.Text())
	}
	return nil
}
" >> $1/$2.go

# Create the test file
echo "package $1

import \"testing\"

func Test_read(t *testing.T) {
	$abbr := $2{}
	if err := $abbr.load(\"test_input.txt\"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len($abbr.input) != 1000 {
		t.Logf(\"Expected 1000 inputs, found %d\", len($abbr.input))
		t.Fail()
	}

}
" >> $1/$2_test.go

# Create files for test input
touch $1/test_input.txt
touch $1/input.txt

# Append Solution to Readme

echo "
### Day $1

\`\`\`sh
$ ./aoc2021 --$1
The solution for \"$1\" is:
$1 part 1 answer for $2
$1 part 2 answer for $2
\`\`\`
" >> README.md