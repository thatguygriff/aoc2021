package main

import (
	"fmt"
	"os"
	"strings"

	"unsupervised.ca/aoc2021/eight"
	"unsupervised.ca/aoc2021/eighteen"
	"unsupervised.ca/aoc2021/eleven"
	"unsupervised.ca/aoc2021/fifteen"
	"unsupervised.ca/aoc2021/five"
	"unsupervised.ca/aoc2021/four"
	"unsupervised.ca/aoc2021/fourteen"
	"unsupervised.ca/aoc2021/nine"
	"unsupervised.ca/aoc2021/nineteen"
	"unsupervised.ca/aoc2021/one"
	"unsupervised.ca/aoc2021/seven"
	"unsupervised.ca/aoc2021/seventeen"
	"unsupervised.ca/aoc2021/six"
	"unsupervised.ca/aoc2021/sixteen"
	"unsupervised.ca/aoc2021/ten"
	"unsupervised.ca/aoc2021/thirteen"
	"unsupervised.ca/aoc2021/three"
	"unsupervised.ca/aoc2021/twelve"
	"unsupervised.ca/aoc2021/two"
)

func main() {
	args := os.Args

	if len(args) != 2 {
		help()
		return
	}

	flagParts := strings.Split(args[1], "--")
	if len(flagParts) != 2 {
		fmt.Println("Unable to read day from flag")
	}

	var day Day
	switch flagParts[1] {
	case "one":
		day = one.Init("one/input.txt")
	case "two":
		day = two.Init("two/input.txt")
	case "three":
		day = three.Init("three/input.txt")
	case "four":
		day = four.Init("four/input.txt")
	case "five":
		day = five.Init("five/input.txt")
	case "six":
		day = six.Init("six/input.txt")
	case "seven":
		day = seven.Init("seven/input.txt")
	case "eight":
		day = eight.Init("eight/input.txt")
	case "nine":
		day = nine.Init("nine/input.txt")
	case "ten":
		day = ten.Init("ten/input.txt")
	case "eleven":
		day = eleven.Init("eleven/input.txt")
	case "twelve":
		day = twelve.Init("twelve/input.txt")
	case "thirteen":
		day = thirteen.Init("thirteen/input.txt")
	case "fourteen":
		day = fourteen.Init("fourteen/input.txt")
	case "fifteen":
		day = fifteen.Init("fifteen/input.txt")
	case "sixteen":
		day = sixteen.Init("sixteen/input.txt")
	case "seventeen":
		day = seventeen.Init("seventeen/input.txt")
	case "eighteen":
		day = eighteen.Init("eighteen/input.txt")
	case "nineteen":
		day = nineteen.Init("nineteen/input.txt")
	default:
		fmt.Printf("%q does not have a solution.\n", flagParts[1])
		help()
		return
	}

	fmt.Printf("The solution for %q is:\n", flagParts[1])
	fmt.Println(day.Answer())
	fmt.Println(day.FollowUp())
}

func help() {
	fmt.Println("To execute thatguygriff's solutions for Advent of Code execute the command with the day as a flag")
	fmt.Println("")
	fmt.Printf("%s [--one|--two|--three]\n", os.Args[0])
	fmt.Println("")
	fmt.Println("If there is no solution for the flagged day this text will be printed.")
}
