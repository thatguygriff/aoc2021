package one

import (
	"bufio"
	"os"
	"strconv"
)

type radar struct {
	input []int
}

// scan Loads the data from input.txt into radar
func (r *radar) scan(filename string) error {
	r.input = []int{}

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		depth, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return err
		}
		r.input = append(r.input, depth)
	}

	return nil
}

// countIncreases for a given array of numbers how many times does one value increment after another
func countIncreases(input []int) int {
	increases := 0
	previousValue := -1

	for _, value := range input {
		if previousValue == -1 {
			previousValue = value
			continue
		}

		if value > previousValue {
			increases++
		}
		previousValue = value
	}
	return increases
}

func (r *radar) depthIncreases() int {
	return countIncreases(r.input)
}

func (r *radar) windowedDepthIncreases() int {
	windowedDepths := []int{}
	for i := 0; i < len(r.input)-2; i++ {
		value := r.input[i] + r.input[i+1] + r.input[i+2]
		windowedDepths = append(windowedDepths, value)
	}

	return countIncreases(windowedDepths)
}
