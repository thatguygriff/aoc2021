package one

import "testing"

var testInput = []int{
	199,
	200,
	208,
	210,
	200,
	207,
	240,
	269,
	260,
	263,
}

func Test_scan(t *testing.T) {
	r := radar{}
	if err := r.scan("input.txt"); err != nil {
		t.FailNow()
	}

	if len(r.input) != 2000 {
		t.Logf("Expected 2000 entries, found %d", len(r.input))
		t.Fail()
	}
}

func Test_depthIncreases(t *testing.T) {
	r := radar{
		input: testInput,
	}

	increases := r.depthIncreases()
	if increases != 7 {
		t.Logf("Expected 7 increases, found %d", increases)
		t.FailNow()
	}
}

func Test_windowDepthIncrease(t *testing.T) {
	r := radar{
		input: testInput,
	}

	increases := r.windowedDepthIncreases()
	if increases != 5 {
		t.Logf("Expected 5 increases, found %d", increases)
		t.FailNow()
	}
}