package one

import "fmt"

type One struct {
	radar radar
}

func Init(filepath string) *One {
	one := &One{
		radar: radar{},
	}

	one.radar.scan(filepath)
	return one
}

func (d *One) Answer() string {
	return fmt.Sprintf("There were %d increases in depth", d.radar.depthIncreases())
}

func (d *One) FollowUp() string {
	return fmt.Sprintf("There were %d increases in depth when sampling a window", d.radar.windowedDepthIncreases())
}
