package main

type Day interface {
	Answer() string
	FollowUp() string
}
