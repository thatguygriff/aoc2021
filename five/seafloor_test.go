package five

import "testing"

func Test_read(t *testing.T) {
	s := seafloor{}
	if err := s.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(s.input) != 10 {
		t.Logf("Expected 10 inputs, found %d", len(s.input))
		t.Fail()
	}
}

func Test_mapVents(t *testing.T) {
	s := seafloor{}
	s.load("test_input.txt")

	overlap, err := s.mapVents(false)

	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if overlap != 5 {
		t.Logf("Expected 5 vent overlaps, found %d", overlap)
		t.Fail()
	}
}

func Test_mapVentsDiagonal(t *testing.T) {
	s := seafloor{}
	s.load("test_input.txt")

	overlap, err := s.mapVents(true)

	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if overlap != 12 {
		t.Logf("Expected 12 vent overlaps, found %d", overlap)
		t.Fail()
	}
}

