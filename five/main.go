package five

import "fmt"

type Five struct {
	seafloor seafloor
}

func Init(filepath string) *Five {
	five := &Five{
		seafloor: seafloor{},
	}

	five.seafloor.load(filepath)
	return five
}

func (d *Five) Answer() string {
	overlap, _ := d.seafloor.mapVents(false)
	return fmt.Sprintln("There are", overlap, "overlapping vents")
}

func (d *Five) FollowUp() string {
	overlap, _ := d.seafloor.mapVents(true)
	return fmt.Sprintln("There are", overlap, "overlapping vents")
}
