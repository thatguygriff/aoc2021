package five

import (
	"bufio"
	"fmt"
	"os"
)

type seafloor struct {
	input   []string
	ventMap map[int]map[int]int
}

func (s *seafloor) load(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		s.input = append(s.input, scanner.Text())
	}
	return nil
}

func (s *seafloor) mapVents(diagonal bool) (int, error) {
	var startX, startY, endX, endY int
	s.ventMap = map[int]map[int]int{}
	for _, vent := range s.input {
		if count, err := fmt.Sscanf(vent, "%d,%d -> %d,%d", &startX, &startY, &endX, &endY); err != nil || count != 4 {
			return -1, fmt.Errorf("scanned %d values: %w", count, err)
		}

		deltaX := endX - startX
		deltaY := endY - startY

		if !diagonal {
			if deltaX != 0 && deltaY != 0 {
				continue
			}
		}

		x := startX
		y := startY
		completed := false
		for !completed {
			if s.ventMap[x] == nil {
				s.ventMap[x] = map[int]int{}
			}
			s.ventMap[x][y]++
			if x == endX && y == endY {
				completed = true
			} else {
				if deltaX > 0 {
					x++
				} else if deltaX < 0 {
					x--
				}

				if deltaY > 0 {
					y++
				} else if deltaY < 0 {
					y--
				}
			}
		}
	}

	overlap := 0

	for xKey := range s.ventMap {
		for yKey := range s.ventMap[xKey] {
			if s.ventMap[xKey][yKey] > 1 {
				overlap++
			}
		}
	}

	return overlap, nil
}
