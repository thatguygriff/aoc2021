package ten

import (
	"testing"
)

func Test_read(t *testing.T) {
	n := navigation{}
	if err := n.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(n.input) != 10 {
		t.Logf("Expected 10 inputs, found %d", len(n.input))
		t.Fail()
	}
}

func Test_parse(t *testing.T) {
	n := navigation{}
	n.load("test_input.txt")

	n.parse()

	corruptedLines := 0
	for _, e := range n.errors {
		if e.violation == corrupted {
			corruptedLines++
		}
	}

	if corruptedLines != 5 {
		t.Logf("Expected 5 corrupt inputs, found %d", corruptedLines)
		t.Fail()
	}
}

func Test_syntaxScore(t *testing.T) {
	n := navigation{}
	n.load("test_input.txt")

	n.parse()

	score := n.syntaxScore()

	if score != 26397 {
		t.Logf("Expected syntax score of 26397, found %d", score)
		t.Fail()
	}
}

func Test_autoCompleteScore(t *testing.T) {
	n := navigation{}
	n.load("test_input.txt")

	n.parse()

	score := n.autoCompleteScore()

	if score != 288957 {
		t.Logf("Expected autoComplete score of 288957, found %d", score)
		t.Fail()
	}
}
