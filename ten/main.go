package ten

import "fmt"

type Ten struct {
	navigation navigation
}

func Init(filepath string) *Ten {
	ten := &Ten{
		navigation: navigation{},
	}

	ten.navigation.load(filepath)
	ten.navigation.parse()
	return ten
}

func (d *Ten) Answer() string {
	return fmt.Sprintf("The syntax error score is %d", d.navigation.syntaxScore())
}

func (d *Ten) FollowUp() string {
	return fmt.Sprintf("The autoComplete score is %d", d.navigation.autoCompleteScore())
}
