package four

import "testing"

func Test_read(t *testing.T) {
	b := bingo{}
	if err := b.read("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(b.calls) != 27 {
		t.Logf("Expected 27 number calls, found %d", len(b.calls))
		t.Fail()
	}

	if len(b.boards) != 3 {
		t.Logf("Expected 3 boards, found %d", len(b.boards))
		t.Fail()
	}
}

func Test_play(t *testing.T) {
	b := bingo{}
	if err := b.read("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	score, call := b.play()

	if score != 188 {
		t.Logf("Expected a winning score of 188, got %d", score)
		t.Fail()
	}

	if call != 24 {
		t.Logf("Expected bingo on call 24, got %d", call)
		t.Fail()
	}

	if score*call != 4512 {
		t.Logf("Expected result of 4512, got %d", score*call)
		t.Fail()
	}
}

func Test_slowPlay(t *testing.T) {
	b := bingo{}
	if err := b.read("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	score, call := b.slowPlay()

	if score != 148 {
		t.Logf("Expected a winning score of 148, got %d", score)
		t.Fail()
	}

	if call != 13 {
		t.Logf("Expected bingo on call 13, got %d", call)
		t.Fail()
	}

	if score*call != 1924 {
		t.Logf("Expected result of 1924, got %d", score*call)
		t.Fail()
	}
}
