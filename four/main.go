package four

import "fmt"

type Four struct {
	bingo bingo
}

func Init(filepath string) *Four {
	four := &Four{
		bingo: bingo{},
	}

	four.bingo.read(filepath)
	return four
}

func (d *Four) Answer() string {
	score, call := d.bingo.play()
	return fmt.Sprintf("The winning score is %d on call %d. Final score %d", score, call, score*call)
}

func (d *Four) FollowUp() string {
	d.bingo.reset()
	score, call := d.bingo.slowPlay()
	return fmt.Sprintf("The last winning score is %d on call %d. Final score %d", score, call, score*call)
}
