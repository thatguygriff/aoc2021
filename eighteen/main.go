package eighteen

import "fmt"

type Eighteen struct {
	snail snail
}

func Init(filepath string) *Eighteen {
	eighteen := &Eighteen{
		snail: snail{},
	}

	eighteen.snail.load(filepath)
	return eighteen
}

func (d *Eighteen) Answer() string {
	sum := d.snail.sum()

	return fmt.Sprintf("The magnitude of the final sum is %d", sum.magnitude())
}

func (d *Eighteen) FollowUp() string {
	return fmt.Sprintf("The largest magnitude of any two snail numbers is %d", d.snail.largestMagnitude())
}
