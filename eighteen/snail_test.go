package eighteen

import (
	"fmt"
	"testing"
)

func Test_read(t *testing.T) {
	s := snail{}
	if err := s.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(s.input) != 10 {
		t.Logf("Expected 10 inputs, found %d", len(s.input))
		t.Fail()
	}
}

func Test_magnitude(t *testing.T) {
	s := snail{}
	s.load("test_input.txt")

	sum := s.sum()

	fmt.Println(sum.print())
	mag := sum.magnitude()

	if mag != 4140 {
		t.Logf("Expected a magnitude of 4140, found %d", mag)
		t.Fail()
	}
}

func Test_magnitude2(t *testing.T) {
	v := parse("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")
	expected := 1384

	mag := v.magnitude()

	if mag != expected {
		t.Logf("Expected magnitude %d, found %d", expected, mag)
		t.Fail()
	}
}

func Test_magnitude3(t *testing.T) {
	v := parse("[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]")
	expected := 4140

	mag := v.magnitude()

	if mag != expected {
		t.Logf("Expected magnitude %d, found %d", expected, mag)
		t.Fail()
	}
}

func Test_largestMagnitude(t *testing.T) {
	s := snail{}
	s.load("test_input.txt")

	mag := s.largestMagnitude()

	if mag != 3993 {
		t.Logf("Expected largest magnitude of 3993, found %d", mag)
		t.Fail()
	}
}

func Test_sum(t *testing.T) {
	s := snail{}
	s.load("test_input2.txt")

	sum := s.sum()
	output := sum.print()
	expected := "[[[[1,1],[2,2]],[3,3]],[4,4]]"

	if expected != output {
		t.Logf("Expected %q, found %q", expected, output)
		t.Fail()
	}
}

func Test_sum2(t *testing.T) {
	s := snail{}
	s.load("test_input3.txt")

	sum := s.sum()
	output := sum.print()
	expected := "[[[[3,0],[5,3]],[4,4]],[5,5]]"

	if expected != output {
		t.Logf("Expected %q, found %q", expected, output)
		t.Fail()
	}
}

func Test_sum3(t *testing.T) {
	s := snail{}
	s.load("test_input4.txt")

	sum := s.sum()
	output := sum.print()
	expected := "[[[[5,0],[7,4]],[5,5]],[6,6]]"

	if expected != output {
		t.Logf("Expected %q, found %q", expected, output)
		t.Fail()
	}
}

func Test_sum4(t *testing.T) {
	s := snail{}
	s.load("test_input5.txt")

	sum := s.sum()
	output := sum.print()
	expected := "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"

	if expected != output {
		t.Logf("Expected %q, found %q", expected, output)
		t.Fail()
	}
}

func Test_sum5(t *testing.T) {
	v1 := parse("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]")
	v2 := parse("[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]")

	r := v1.add(v2)
	expected := "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]"
	output := r.print()

	if expected != output {
		t.Logf("Expected %q, found %q", expected, output)
		t.Fail()
	}
}

func Test_reduce(t *testing.T) {
	v := parse("[[[[[9,8],1],2],3],4]")

	r := v.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.FailNow()
	}

	r = r.reduce()
	if r != nil {
		t.Logf("Only expected a single reduction, found %s", r.print())
		t.Fail()
	}
}

func Test_reduce2(t *testing.T) {
	v := parse("[7,[6,[5,[4,[3,2]]]]]")

	r := v.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.FailNow()
	}

	r = r.reduce()
	if r != nil {
		t.Logf("Only expected a single reduction, found %s", r.print())
		t.Fail()
	}
}

func Test_reduce3(t *testing.T) {
	v := parse("[[6,[5,[4,[3,2]]]],1]")

	r := v.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.FailNow()
	}

	r = r.reduce()
	if r != nil {
		t.Logf("Only expected a single reduction, found %s", r.print())
		t.Fail()
	}
}

func Test_reduce4(t *testing.T) {
	v := parse("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]")

	r := v.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.FailNow()
	}

	r = r.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.Fail()
	}

	r = r.reduce()
	if r != nil {
		t.Logf("Only expected two reductions, found %s", r.print())
		t.Fail()
	}
}

func Test_reduce5(t *testing.T) {
	v := parse("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]")

	r := v.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.FailNow()
	}

	r = r.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.Fail()
	}

	r = r.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.Fail()
	}

	r = r.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.Fail()
	}

	r = r.reduce()
	if r == nil {
		t.Logf("Expected a reduction, didn't get one")
		t.Fail()
	}

	r = r.reduce()
	if r != nil {
		t.Logf("Only expected five reductions, found %s", r.print())
		t.Fail()
	}
}

func Test_add(t *testing.T) {
	first := parse("[1,2]")
	second := parse("[[3,4],5]")

	sum := first.add(second)

	// Check that it equals [[1,2],[[3,4],5]]
	if sum.p == nil || sum.p.left.p.left.v != 1 || sum.p.left.p.right.v != 2 {
		t.Logf("Expected [1,2], found %s", sum.p.left.print())
		t.Fail()
	}

	// Check that it equals [[1,2],[[3,4],5]]
	if sum.p == nil || sum.p.right.p.right.v != 5 {
		t.Logf("Expected [[3,4],5], found %s", sum.p.left.print())
		t.Fail()
	}
}

func Test_parse(t *testing.T) {
	s := "[1,2]"

	v := parse(s)
	if v.p == nil {
		t.Logf("Expected a pair, found nothing")
		t.FailNow()
	}

	if v.p.left.v != 1 {
		t.Logf("Expected 1, found %d", v.p.left.v)
		t.Fail()
	}

	if v.p.right.v != 2 {
		t.Logf("Expected 2, found %d", v.p.right.v)
		t.FailNow()
	}
}

func Test_parse3(t *testing.T) {
	s := "[9,[8,7]]"

	v := parse(s)
	if v.p == nil {
		t.Logf("Expected a pair, found nothing")
		t.FailNow()
	}

	if v.p.right.p.left.v != 8 {
		t.Logf("Expected 8, found %d", v.p.right.p.left.v)
		t.Fail()
	}

	if v.p.right.p.right.v != 7 {
		t.Logf("Expected 7, found %d", v.p.right.p.right.v)
		t.Fail()
	}

	if v.p.left.v != 9 {
		t.Logf("Expected 9, found %d", v.p.left.v)
		t.FailNow()
	}
}

func Test_parse2(t *testing.T) {
	s := "[[1,2],3]"

	v := parse(s)
	if v.p == nil {
		t.Logf("Expected a pair, found nothing")
		t.FailNow()
	}

	if v.p.left.p.left.v != 1 {
		t.Logf("Expected 1, found %d", v.p.left.p.left.v)
		t.Fail()
	}

	if v.p.left.p.right.v != 2 {
		t.Logf("Expected 2, found %d", v.p.left.p.right.v)
		t.Fail()
	}

	if v.p.right.v != 3 {
		t.Logf("Expected 3, found %d", v.p.right.v)
		t.FailNow()
	}
}
