package two

import (
	"strconv"
	"strings"
	"testing"
)

var testInput = []string{
	"forward 5",
	"down 5",
	"forward 8",
	"up 3",
	"down 8",
	"forward 2",
}

func Test_scan(t *testing.T) {
	s := sub{}
	if err := s.load("input.txt"); err != nil {
		t.FailNow()
	}

	if len(s.input) != 1000 {
		t.Logf("Expected 1000 commands, found %d", len(s.input))
		t.Fail()
	}
}

func Test_execute(t *testing.T) {
	s := sub{
		input: []command{},
	}

	for _, raw := range testInput {
		parts := strings.Split(raw, " ")
		value, _ := strconv.Atoi(parts[1])
		s.input = append(s.input, command{
			instruction: parts[0],
			value:       value,
		})
	}

	s.execute()
	if s.horizontal != 15 {
		t.Logf("Expected horizontal of 15, found %d", s.horizontal)
		t.FailNow()
	}

	if s.depth != 10 {
		t.Logf("Expected depth of 10, found %d", s.depth)
		t.FailNow()
	}
}

func Test_executeWithAim(t *testing.T) {
	s := sub{
		input: []command{},
	}

	for _, raw := range testInput {
		parts := strings.Split(raw, " ")
		value, _ := strconv.Atoi(parts[1])
		s.input = append(s.input, command{
			instruction: parts[0],
			value:       value,
		})
	}

	s.executeWithAim()
	if s.horizontal != 15 {
		t.Logf("Expected horizontal of 15, found %d", s.horizontal)
		t.FailNow()
	}

	if s.depth != 60 {
		t.Logf("Expected depth of 60, found %d", s.depth)
		t.FailNow()
	}
}
