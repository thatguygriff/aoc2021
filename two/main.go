package two

import "fmt"

type Two struct {
	sub sub
}

func Init(filepath string) *Two {
	two := &Two{
		sub: sub{},
	}

	two.sub.load(filepath)

	return two
}

func (d *Two) Answer() string {
	d.sub.reset()
	d.sub.execute()

	return fmt.Sprintf("Horizontal is %d, depth is %d, answer is %d",
		d.sub.horizontal,
		d.sub.depth,
		d.sub.horizontal*d.sub.depth,
	)
}

func (d *Two) FollowUp() string {
	d.sub.reset()
	d.sub.executeWithAim()

	return fmt.Sprintf("Horizontal is %d, depth is %d, answer is %d",
		d.sub.horizontal,
		d.sub.depth,
		d.sub.horizontal*d.sub.depth,
	)
}
