package eight

import "fmt"

type Eight struct {
	display display
}

func Init(filepath string) *Eight {
	eight := &Eight{
		display: display{},
	}

	eight.display.load(filepath)
	return eight
}

func (d *Eight) Answer() string {
	count := d.display.uniqueOuputs()
	return fmt.Sprintf("There were %d instances of digits 1, 4, 7, or 8", count)
}

func (d *Eight) FollowUp() string {
	outputSum := d.display.outputSum()
	return fmt.Sprintf("The sum of the decoded output values was %d", outputSum)
}
