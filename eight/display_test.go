package eight

import "testing"

func Test_read(t *testing.T) {
	d := display{}
	if err := d.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(d.outputs) != 10 {
		t.Logf("Expected 10 input sets, found %d", len(d.outputs))
		t.Fail()
	}

	if len(d.outputs[0].signals) != 10 {
		t.Logf("Expected 10 inputs, found %d", len(d.outputs[0].signals))
		t.Fail()
	}

	if len(d.outputs[0].outputs) != 4 {
		t.Logf("Expected 4 outputs, found %d", len(d.outputs[0].outputs))
		t.Fail()
	}
}

func Test_uniqueOutputs(t *testing.T) {
	d := display{}
	d.load("test_input.txt")

	if count := d.uniqueOuputs(); count != 26 {
		t.Logf("Expected 26 unique output digits, found %d", count)
		t.Fail()
	}
}

func Test_decode(t *testing.T) {
	d := display{}
	d.load("test_input.txt")

	o := d.outputs[0]

	if decoded := o.decode(); decoded != 8394 {
		t.Logf("Expected output of 8394, decoded %d", decoded)
		t.Fail()
	}
}

func Test_outputSum(t *testing.T) {
	d := display{}
	d.load("test_input.txt")

	if sum := d.outputSum(); sum != 61229 {
		t.Logf("Expected outputs to sum to 61229, calculated %d", sum)
		t.Fail()
	}
}