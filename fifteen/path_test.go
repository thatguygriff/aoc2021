package fifteen

import "testing"

func Test_read(t *testing.T) {
	p := path{}
	if err := p.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(p.riskLevel) != 10 {
		t.Logf("Expected 10 rows, found %d", len(p.riskLevel))
		t.Fail()
	}

	if len(p.riskLevel[0]) != 10 {
		t.Logf("Expected 10 rows, found %d", len(p.riskLevel[0]))
		t.Fail()
	}
}

func Test_totalRisk(t *testing.T) {
	p := path{}
	p.load("test_input.txt")

	if value := p.totalRisk(); value != 40 {
		t.Logf("Expected a total risk of 40, found %d", value)
		t.Fail()
	}
}

func Test_totalRiskScaled(t *testing.T) {
	p := path{}
	p.load("test_input.txt")
	p = *p.scale(5, 5)

	p.print()

	if value := p.totalRisk(); value != 315 {
		t.Logf("Expected a total risk of 315, found %d", value)
		t.Fail()
	}
}