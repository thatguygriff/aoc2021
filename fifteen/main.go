package fifteen

import "fmt"

type Fifteen struct {
	path path
}

func Init(filepath string) *Fifteen {
	fifteen := &Fifteen{
		path: path{},
	}

	fifteen.path.load(filepath)
	return fifteen
}

func (d *Fifteen) Answer() string {
	return fmt.Sprintf("The total risk of the least risky path is %d", d.path.totalRisk())
}

func (d *Fifteen) FollowUp() string {
	scaled := d.path.scale(5, 5)
	return fmt.Sprintf("The total risk of the least risky path is %d", scaled.totalRisk())
}
