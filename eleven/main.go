package eleven

import "fmt"

type Eleven struct {
	cavern cavern
}

func Init(filepath string) *Eleven {
	eleven := &Eleven{
		cavern: cavern{},
	}

	eleven.cavern.load(filepath)
	return eleven
}

func (d *Eleven) Answer() string {
	return fmt.Sprintf("There were %d flashes after 100 steps", d.cavern.totalFlashes(100))
}

func (d *Eleven) FollowUp() string {
	return fmt.Sprintf("They all flash on step %d", d.cavern.stepsToSynchronousFlash())
}
