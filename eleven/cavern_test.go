package eleven

import (
	"testing"
)

func Test_read(t *testing.T) {
	c := cavern{}
	if err := c.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(c.octopodes) != 10 {
		t.Logf("Expected 10 rows, found %d", len(c.octopodes))
		t.Fail()
	}

	if len(c.octopodes[0]) != 10 {
		t.Logf("Expected 10 columns, found %d", len(c.octopodes))
		t.Fail()
	}
}

func Test_flashes(t *testing.T) {
	c := cavern{}
	c.load("test_input.txt")

	flashes := c.totalFlashes(1)

	if flashes != 0 {
		t.Logf("Expected 0 flashes, got %d", flashes)
		t.Fail()
	}

	flashes = c.totalFlashes(2)

	if flashes != 35 {
		t.Logf("Expected 35 flashes, got %d", flashes)
		t.Fail()
	}

	flashes = c.totalFlashes(10)

	if flashes != 204 {
		t.Logf("Expected 204 flashes, got %d", flashes)
		t.Fail()
	}

	flashes = c.totalFlashes(100)

	if flashes != 1656 {
		t.Logf("Expected 1656 flashes, got %d", flashes)
		t.Fail()
	}
}

func Test_stepsToSynchronousFlash(t *testing.T) {
	c := cavern{}
	c.load("test_input.txt")

	steps := c.stepsToSynchronousFlash()

	if steps != 195 {
		t.Logf("Expected it to take 195 steps, found %d", steps)
		t.Fail()
	}
}
