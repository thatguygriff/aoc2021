package sixteen

import "fmt"

type Sixteen struct {
	packet packet
}

func Init(filepath string) *Sixteen {
	sixteen := &Sixteen{
		packet: packet{},
	}

	sixteen.packet = load(filepath)
	return sixteen
}

func (d *Sixteen) Answer() string {
	return fmt.Sprintf("The sum of the version numbers is %d", d.packet.versionSum())
}

func (d *Sixteen) FollowUp() string {
	return fmt.Sprintf("The output of the encoded input is %d", d.packet.value())
}
