package sixteen

import (
	"fmt"
	"testing"
)

func Test_hex(t *testing.T) {
	original := "8A004A801A8002F478"
	expected := 16

	p := parseHex(original)
	vSum := p.versionSum()

	if vSum != expected {
		t.Logf("Expected %d, found %d", expected, vSum)
		t.Fail()
	}
}

func Test_hex2(t *testing.T) {
	original := "620080001611562C8802118E34"
	expected := 12

	p := parseHex(original)
	vSum := p.versionSum()

	if vSum != expected {
		t.Logf("Expected %d, found %d", expected, vSum)
		t.Fail()
	}
}

func Test_hex3(t *testing.T) {
	original := "C0015000016115A2E0802F182340"
	expected := 23

	p := parseHex(original)
	vSum := p.versionSum()

	if vSum != expected {
		t.Logf("Expected %d, found %d", expected, vSum)
		t.Fail()
	}
}

func Test_hex4(t *testing.T) {
	original := "A0016C880162017C3686B18A3D4780"
	expected := 31

	p := parseHex(original)
	vSum := p.versionSum()

	if vSum != expected {
		t.Logf("Expected %d, found %d", expected, vSum)
		t.Fail()
	}
}

func Test_type4(t *testing.T) {
	b := parse("110100101111111000101000")

	if b.version != 6 {
		t.Logf("Expected version 6, found %d", b.version)
		t.Fail()
	}

	if b.typeID != 4 {
		t.Logf("Expected type 4, found %d", b.typeID)
		t.Fail()
	}

	if b.content != 2021 {
		t.Logf("Expected content to be 2021, found %d", b.content)
		t.Fail()
	}
}

func Test_operator(t *testing.T) {
	b := parse("00111000000000000110111101000101001010010001001000000000")

	if b.version != 1 {
		t.Logf("Expected version 1, found %d", b.version)
		t.Fail()
	}

	if b.typeID != 6 {
		t.Logf("Expected type 6, found %d", b.typeID)
		t.Fail()
	}

	if len(b.subpackets) != 2 {
		t.Logf("Expected 2 subpacket, found %d", len(b.subpackets))
		t.Fail()
	}

	if b.subpackets[0].content != 10 {
		t.Logf("Expected first packet to be 10, found %d", b.subpackets[0].content)
		t.Fail()
	}

	if b.subpackets[1].content != 20 {
		t.Logf("Expected second packet to be 20, found %d", b.subpackets[2].content)
		t.Fail()
	}
}

func Test_operator2(t *testing.T) {
	b := parse("11101110000000001101010000001100100000100011000001100000")

	if b.version != 7 {
		t.Logf("Expected version 1, found %d", b.version)
		t.Fail()
	}

	if b.typeID != 3 {
		t.Logf("Expected type 6, found %d", b.typeID)
		t.Fail()
	}

	if len(b.subpackets) != 3 {
		t.Logf("Expected 3 subpacket, found %d", len(b.subpackets))
		t.FailNow()
	}

	if b.subpackets[0].content != 1 {
		t.Logf("Expected first packet to be 1, found %d", b.subpackets[0].content)
		t.Fail()
	}

	if b.subpackets[1].content != 2 {
		t.Logf("Expected second packet to be 2, found %d", b.subpackets[2].content)
		t.Fail()
	}

	if b.subpackets[2].content != 3 {
		t.Logf("Expected third packet to be 3, found %d", b.subpackets[2].content)
		t.Fail()
	}
}

func Test_sum(t *testing.T) {
	original := "C200B40A82"
	expected := 3

	p := parseHex(original)
	fmt.Println(p)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_product(t *testing.T) {
	original := "04005AC33890"
	expected := 54

	p := parseHex(original)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_min(t *testing.T) {
	original := "880086C3E88112"
	expected := 7

	p := parseHex(original)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_max(t *testing.T) {
	original := "CE00C43D881120"
	expected := 9

	p := parseHex(original)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_less(t *testing.T) {
	original := "D8005AC2A8F0"
	expected := 1

	p := parseHex(original)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_more(t *testing.T) {
	original := "F600BC2D8F"
	expected := 0

	p := parseHex(original)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_equal(t *testing.T) {
	original := "9C005AC2F8F0"
	expected := 0

	p := parseHex(original)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_combo(t *testing.T) {
	original := "9C0141080250320F1802104A08"
	expected := 1

	p := parseHex(original)
	v := p.value()

	if v != expected {
		t.Logf("Expected %d, found %d", expected, v)
		t.Fail()
	}
}

func Test_input(t *testing.T) {
	expected := 531986792411

	p := load("input.txt")

	v := p.value()

	if v == expected {
		t.Logf("Expected to not get %d, got %d", expected, v)
		t.Fail()
	}
}