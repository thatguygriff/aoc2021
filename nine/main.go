package nine

import "fmt"

type Nine struct {
	vents vents
}

func Init(filepath string) *Nine {
	nine := &Nine{
		vents: vents{},
	}

	nine.vents.load(filepath)
	return nine
}

func (d *Nine) Answer() string {
	return fmt.Sprintf("The sum of the risk level of low points is %d", d.vents.sumLowPointRisk())
}

func (d *Nine) FollowUp() string {
	return fmt.Sprintf("The product of the three largest basins is %d", d.vents.productLargestBasins())
}
