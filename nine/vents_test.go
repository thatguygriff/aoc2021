package nine

import "testing"

func Test_read(t *testing.T) {
	v := vents{}
	if err := v.load("test_input.txt"); err != nil {
		t.Log(err)
		t.FailNow()
	}

	if len(v.floor) != 5 {
		t.Logf("Expected 5 rows, found %d", len(v.floor))
		t.Fail()
	}

}

func Test_lowPoints(t *testing.T) {
	v := vents{}
	v.load("test_input.txt")

	lowPoints := v.findLowPoints()

	if len(lowPoints) != 4 {
		t.Logf("Expected 4 low points, found %d", len(lowPoints))
	}
}

func Test_sumLowPointRisk(t *testing.T) {
	v := vents{}
	v.load("test_input.txt")

	sum := v.sumLowPointRisk()
	if sum != 15 {
		t.Logf("Expected risk level of 15 rows, found %d", sum)
		t.Fail()
	}
}

func Test_productLargestBasins(t *testing.T) {
	v := vents{}
	v.load("test_input.txt")

	product := v.productLargestBasins()
	if product != 1134 {
		t.Logf("Expected largest basin product of 1134, found %d", product)
		t.Fail()
	}
}
